# Project description
This is a frontend for [CRUD app](https://gitlab.com/pskrajnyy/CRUD-app)

# Test it live
The application has been deployed on [Heroku](https://www.heroku.com/). You can test it by yourself on this [website](https://pskrajnyy.gitlab.io/crud-app-frontend/).